package com.example.au;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
class SquizChallengeApplicationTests {

	@Test
	void contextLoads() {
	}

	private String processCsv = "/processCsv";
	private String processJson = "/processJson";
	private String pageLimit = "pageLimit";

	@Autowired
	private MockMvc mvc;

	// controller endpoint tests
	@Test
	public void whenCsvPageSizeGreaterThan100_thenThrowError() {
		try {
			mvc.perform(get(processCsv).param(pageLimit, "101")).andExpect(status().isInternalServerError());
		} catch (Exception e) {
			System.out.println("Please re-check the logic :" + e);
		}
	}

	@Test
	public void whenJsonPageSizeGreaterThan100_thenThrowError() {
		try {
			mvc.perform(get(processJson).param(pageLimit, "101")).andExpect(status().isInternalServerError());
		} catch (Exception e) {
			System.out.println("Please re-check the logic :" + e);
		}
	}

}
