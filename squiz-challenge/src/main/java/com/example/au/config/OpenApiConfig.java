package com.example.au.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class OpenApiConfig {
	// config for Swagger api documentation
	@Bean
	public OpenAPI customOpenAPI() {
		Info info = new Info();
		info.setTitle("Squiz Backend Challenge");
		return new OpenAPI().info(info);
	}
}
