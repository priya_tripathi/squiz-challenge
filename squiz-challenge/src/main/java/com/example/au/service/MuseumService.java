package com.example.au.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.example.au.models.Museum;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Priya Tripathi
 *
 */
@Service
public class MuseumService {
	private static final String SAMPLE_JSON = "classpath:musuem-data.json";
	private static final String SAMPLE_CSV = "classpath:museums.csv";

	private static final Logger logger = LoggerFactory.getLogger(MuseumService.class);

	@Value("${com.example.au.basePath}")
	private static String basePath;

	public List<List<String>> handleCsv(@Min(0) Long pageOffset, @Min(1) @Max(1000) Long pageLimit) throws IOException {
		logger.info("Initializing csv file type processing");
		List<List<String>> museumList;

		Path filePath = ResourceUtils.getFile(SAMPLE_CSV).toPath();

		try (Stream<String> rows = Files.lines(filePath)) {
			museumList = new ArrayList<>();
			museumList = rows.skip(pageOffset).limit(pageLimit).map(row -> Arrays.asList(row.split(",")))
					.collect(Collectors.toList());
		}
		logger.info("Successfully processed after " + pageOffset + " which includes " + pageLimit + " rows ");
		return museumList;
	}

	
	public List<Museum> handleJson(@Min(0) Long pageOffset, @Min(1) @Max(1000) Long pageLimit) throws IOException {
		logger.info("Initializing json file processing");
		List<Museum> museumList;
		Path filePath = ResourceUtils.getFile(SAMPLE_JSON).toPath();

		try (Stream<String> dictionaries = Files.lines(filePath)) {
			ObjectMapper mapper = new ObjectMapper();

			museumList = dictionaries.skip(pageOffset).limit(pageLimit)
					.map(dictionary -> convertor(mapper, dictionary, Museum.class)).filter(Objects::nonNull)
					.collect(Collectors.toList());
		}
		logger.info("Successfully processed after " + pageOffset + " which includes " + pageLimit + " rows ");
		return museumList;
	}

	private static <T> T convertor(ObjectMapper mapper, String dictionary, Class<T> museumModel) {
		T data = null;
		try {
			data = mapper.readValue(dictionary, museumModel);
		} catch (IOException e) {
			logger.error("Error while mapping data" + e);
		}

		return data;
	}

}
