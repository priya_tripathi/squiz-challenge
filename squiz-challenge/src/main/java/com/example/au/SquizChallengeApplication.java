package com.example.au;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SquizChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SquizChallengeApplication.class, args);
	}

}
