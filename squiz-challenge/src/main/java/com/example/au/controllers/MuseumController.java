package com.example.au.controllers;

import java.io.IOException;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.au.models.Museum;
import com.example.au.service.MuseumService;

@Controller
@RequestMapping("/")
public class MuseumController {

	@Autowired
	MuseumService museumService;

	/**
	 * Perform pagination on a csv file
	 * 
	 * @param pageOffset
	 * @param pageLimit  - page size
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@GetMapping(path = "processCsv", produces = "application/json")
	public List<List<String>> processCsv(
			@RequestParam(name = "pageOffset", required = true, defaultValue = "0") @Min(0) Long pageOffset,
			@RequestParam(name = "pageLimit", required = true, defaultValue = "10") @Min(1) @Max(1000) Long pageLimit)
			throws IOException {
		return museumService.handleCsv(pageOffset, pageLimit);
	}

	/**
	 * Perform pagination on a json file
	 * 
	 * 
	 * @param pageOffset
	 * @param pageLimit  - page size
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@GetMapping(path = "processJson", produces = "application/json")
	public List<Museum> processJson(
			@RequestParam(name = "pageOffset", required = true, defaultValue = "0") @Min(0) Long pageOffset,
			@RequestParam(name = "pageLimit", required = true, defaultValue = "10") @Min(1) @Max(1000) Long pageLimit)
			throws IOException {
		return museumService.handleJson(pageOffset, pageLimit);
	}

}
