package com.example.au.models;

public class Museum {

	private String museumName;
	private String museumType;
	private String museumInstitution;
	private String city;
	private States state;
	private Long taxPeriod;
	private Long income;
	private Long revenue;
	
	
	public String getMuseumName() {
		return museumName;
	}
	public void setMuseumName(String museumName) {
		this.museumName = museumName;
	}
	public String getMuseumType() {
		return museumType;
	}
	public void setMuseumType(String museumType) {
		this.museumType = museumType;
	}
	public String getMuseumInstitution() {
		return museumInstitution;
	}
	public void setMuseumInstitution(String museumInstitution) {
		this.museumInstitution = museumInstitution;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public States getState() {
		return state;
	}
	public void setState(States state) {
		this.state = state;
	}
	public Long getTaxPeriod() {
		return taxPeriod;
	}
	public void setTaxPeriod(Long taxPeriod) {
		this.taxPeriod = taxPeriod;
	}
	public Long getIncome() {
		return income;
	}
	public void setIncome(Long income) {
		this.income = income;
	}
	public Long getRevenue() {
		return revenue;
	}
	public void setRevenue(Long revenue) {
		this.revenue = revenue;
	}
	
}
